using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    [SerializeField] private float speed = 2f;
    [SerializeField] private float jampForce = 100f;
    [SerializeField] private KeyCode klim = KeyCode.Space;   
    [SerializeField] private Collider2D feetCollider;
    [SerializeField] private string groundLayer = "Ground";
  
    private Rigidbody2D playerRigidbody;
    private Animator playerAnimator;
    private SpriteRenderer playerSR;
    private bool isGrounded;

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerSR = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        flip(playerInput);
        isGrounded = feetCollider.IsTouchingLayers(LayerMask.GetMask(groundLayer));
        if (Input.GetKeyDown(klim) && isGrounded)
        {
            Jump();
        }

    }

    private void Move(float direction)
    { 
            playerAnimator.SetBool("Run", direction != 0);
        
            playerRigidbody.velocity = new Vector2(direction * speed, playerRigidbody.velocity.y);
     }

    private void Jump()
    {
        Vector2 jumpVector = new Vector2(playerRigidbody.velocity.x, jampForce);
        playerRigidbody.velocity += jumpVector;
    }

    private void flip(float direction)
    {
        if(direction > 0)
        {
            playerSR.flipX = false;
        }
        if (direction < 0)
        {
            playerSR.flipX = true;
        }
    }
}

