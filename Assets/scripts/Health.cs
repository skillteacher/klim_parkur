using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    //privat int lives = 1;
    private Vector3 startPosition;
    private void Start()
    {
        startPosition = transform.position;
    }

    public void  TakeDamage()
    {
        Game.game.LoseLive();
        transform.position = startPosition;
    }
}
